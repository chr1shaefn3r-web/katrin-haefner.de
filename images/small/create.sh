#!/bin/sh

set -e
set -x

cp ../pic*.jpg ./
mogrify -resize 278 -quality 80% *.jpg
jpegoptim  --strip-all --all-progressive *.jpg
mogrify -format webp -quality 80% *.jpg

# rm *.jpg~

