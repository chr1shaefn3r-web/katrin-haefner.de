#!/usr/bin/env node
"use strict";

var v = {};
v.version = process.env.npm_package_version;
v.name = process.env.npm_package_name;
v.deployer = {
	name: process.env.USER,
	machine: require("os").hostname()
};
v.repo = {
	url: process.env.npm_package_repository_url,
	gitHead: process.env.npm_package_gitHead
};

process.stdout.write(JSON.stringify(v)+"\n");
